from math import *
from graphics import *
from time import sleep
Vo=80
t=0
angulo=70
angulo=angulo*3.1415926535/180

y=0
g=9.8

ventana=GraphWin("simulator",500,500)
ventana.setCoords(0,0,400,400)

while (y>=0):
    x=Vo*cos(angulo)*t
    y=Vo*sin(angulo)*t -(1.0/2)*g*t*t
    micirculo=Circle(Point(x,y),5)
    micirculo.setFill('yellow')
    micirculo.draw(ventana)
    print(x,y)
    sleep(0.1)
    t=t+0.5


ventana.getMouse()
